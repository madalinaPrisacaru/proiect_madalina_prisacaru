## Wantsome - Hotel Reservation System

### 1. Description

This is an application which allows a hotel employee (at the reception) to book hotel rooms.

Supported actions:
- login/register page for multi-user; user must login at start;
- add/update/delete reservations
- add/update/delete customers
- add/update/delete rooms
- view all reservations or only the active/paid/canceled one 
- sort the reservations by date ascendant or descendant
- search the reservations by check-in date and check-out date
- search reservations by reservations number or room number
- view all rooms or only the single/double/triple/presidential/penthouse one (not completed)
- search customers by last name or email
- search for available rooms
- check if a reservation exists for a specified guest (by id)
- show reservation details
- compute invoice: the price to be paid (based on number of days and room type)
- cancel reservation (delete from db, only if start in future)
- delete rooms only if is available(if room is occupied can't to delete)
- list of available rooms
---

### 2. Setup

No setup needed, just start the application. If the database is missing
(like on first startup), it will create a new database (of type SQLite, stored in a local file named 'reservations.db'), and
use it to save the future data.

Once started, access it with a web browser at: <http://localhost:8080>

---

### 3. Technical details

__User interface__

The project includes 1 type of user interface:

- web app (started with ReservationsWebApp class)


__Technologies__

- main code is written in Java (version 11)
- it uses [SQLite](https://www.sqlite.org/), a small embedded database, for its persistence, using SQL and JDBC to
  access it from Java code
- it uses [Javalin](https://javalin.io/) micro web framework (which includes an embedded web server, Jetty)
- it uses [Velocity](https://velocity.apache.org/) templating engine, to separate the UI code from Java code; UI code
  consists of basic HTML and CSS code (and some Javascript)


__Code structure__

- java code is organized in packages by its role, on layers:
    - db - database part, including DTOs and DAOs, as well as the code to init and connect to the db
    - ui - code related to the interface/presentation layer
    - root package - the main classes for the 1 type of interfaces it supports

- web resources are found in `main/resources` folder:
    - under `/public` folder - static resources to be served by the web server directly (images, css files)
    - all other (directly under `/resources`) - the Velocity templates

Note: the focus of this project is on the back-end part, not so much on the front-end part.

---

### 4. Future plans

Other features which could be added in the future:

- JUnit Test

