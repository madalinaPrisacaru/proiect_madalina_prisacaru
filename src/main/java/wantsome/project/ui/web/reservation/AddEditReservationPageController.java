package wantsome.project.ui.web.reservation;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.reservations.State;
import wantsome.project.db.reservations.reservationDao.ReservationDao;
import wantsome.project.db.reservations.reservationDto.ReservationDto;
import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.db.rooms.RoomWithTypeDto;
import wantsome.project.db.rooms.roomDao.RoomDao;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


public class AddEditReservationPageController {

    private final ReservationDao reservationDao;

    public AddEditReservationPageController() {
        reservationDao = new ReservationDao();
    }

    static {
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(State.class, s -> s != null && !s.isEmpty() ? State.valueOf(s) : null);
        JavalinValidation.register(RoomTypeEnum.class, s -> s != null && !s.isEmpty() ? RoomTypeEnum.valueOf(s) : null);
    }

    public void showAddReservationPage(Context ctx) {
        renderAddUpdateForm(ctx, -1, ctx.pathParamAsClass("customerId", Integer.class).get(), null, null,
                -1, "", State.valueOf("ACTIVE"));

    }

    public void showUpdateReservationPage(Context ctx) {

        int id = ctx.pathParamAsClass("id", Integer.class).get();
        Optional<ReservationDto> reservationDto = reservationDao.loadById(id);
        ReservationDto reservation = reservationDto.orElseThrow(() -> new RuntimeException("Reservation with id " + id + " not found!"));
        renderAddUpdateForm(ctx,
                reservation.getId(),
                reservation.getCustomerId(),
                reservation.getStartDate(),
                reservation.getEndDate(),
                reservation.getRoomId(),
                reservation.getExtra_info(),
                reservation.getState());


    }

    /**
     * Render the add or update form. If id>=0, we are on 'update' case (else on 'add')
     */

    private void renderAddUpdateForm(Context ctx, int id, int customerId, Date startDate, Date endDate,
                                     int roomId, String extraInfo, State state) {

        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate", id >= 0);

        model.put("prevId", id);
        model.put("prevCustomerId", customerId);
        model.put("prevStartDate", startDate);
        model.put("prevEndDate", endDate);
        model.put("prevRoomId", roomId);
        model.put("prevExtraInfo", extraInfo);
        model.put("prevState", state);


        RoomDao roomDao = new RoomDao();

        List<RoomWithTypeDto> roomID = roomDao.roomAvailability();

        model.put("reservationOptions", reservationDao.loadAll(null, null));

        model.put("stateOptions", State.values());

        model.put("daysUntilEndDate", computeDaysUntilEndDate(endDate));

        model.put("roomIdList", roomID);

        ctx.render("add_edit_reservation.vm", model);


    }

    private static long computeDaysUntilEndDate(Date date) {
        if (date != null) {
            try {
                LocalDateTime today = LocalDate.now().atStartOfDay();
                LocalDateTime endDate = date.toLocalDate().atStartOfDay();
                return Duration.between(today, endDate).toDays();
            } catch (Exception e) {
                System.err.println("Error computing days until due date '" + date + "': " + e.getMessage());
            }
        }
        return 0;
    }

    public void updateReservation(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).getOrDefault(-1);
        Date startDate = ctx.formParamAsClass("startDate", Date.class).get();
        Date endDate = ctx.formParamAsClass("endDate", Date.class).get();
        int roomId = ctx.formParamAsClass("roomId", Integer.class).get();
        String extraInfo = ctx.formParamAsClass("extraInfo", String.class).allowNullable().get();
        State state = ctx.formParamAsClass("state", State.class).get();

        reservationDao.update(id, startDate, endDate, roomId, extraInfo, state);
        ctx.redirect("/reservations");
    }

    public void addReservation(Context ctx) {
        int customerId = ctx.pathParamAsClass("customerId", Integer.class).get();
        Date startDate = ctx.formParamAsClass("startDate", Date.class).get();
        Date endDate = ctx.formParamAsClass("endDate", Date.class).get();
        int roomId = ctx.formParamAsClass("roomId", Integer.class).get();
        String extraInfo = ctx.formParamAsClass("extraInfo", String.class).allowNullable().get();
        State state = ctx.formParamAsClass("state", State.class).get();

        reservationDao.insertReservation(new ReservationDto(-1, customerId, startDate, endDate, roomId, extraInfo, state));
        ctx.redirect("/reservations");
    }

}
