package wantsome.project.ui.web.reservation;

import io.javalin.http.Context;
import wantsome.project.db.customers.customerDao.CustomerReservationDao;
import wantsome.project.db.customers.customerDto.CustomerReservationDto;
import wantsome.project.db.reservations.*;
import wantsome.project.db.reservations.reservationDao.ReservationDao;
import wantsome.project.db.reservations.reservationDto.ReservationDto;
import wantsome.project.db.reservations.reservationDto.ReservationFullDetails;
import wantsome.project.db.rooms.*;
import wantsome.project.db.rooms.roomDao.RoomDao;
import wantsome.project.db.rooms.roomDao.RoomTypeDao;
import wantsome.project.db.rooms.roomDto.RoomDto;
import wantsome.project.db.rooms.roomDto.RoomTypeDto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ReservationPageController {

    private final RoomDao roomDao;
    private final RoomTypeDao roomTypeDao;
    CustomerReservationDao customerReservationDao;
    private final ReservationDao reservationDao;

    public ReservationPageController() {
        reservationDao = new ReservationDao();
        roomDao = new RoomDao();
        roomTypeDao = new RoomTypeDao();
        customerReservationDao = new CustomerReservationDao();
    }


    /**
     * Controller for the Reservation page - displaying the list of reservations, with sorting/filtering options
     * <p>
     * Reservation that the small pages for add/edit reservation have a separate controller: {@link AddEditReservationPageController}
     */

    public void showReservationPage(Context ctx) {

        Map<String, Object> model = new LinkedHashMap<>();

        String searchText = ctx.queryParam("searchText");
        String searchField = ctx.queryParam("searchField");
        String strCustomerId = ctx.queryParam("customerId");

        List<ReservationFullDetails> reservationList;

        if (searchText != null && searchField != null) {
            SearchByField searchByField = SearchByField.valueOf(searchField);
            reservationList = reservationDao.search(searchByField, searchText);
            setSearchByFieldAndSearchTextValues(ctx, model);
        } else {

            String orderParam = ctx.queryParam("order");
            OrderSortingType orderSortingType;

            if (orderParam != null) {
                orderSortingType = OrderSortingType.valueOf(orderParam);
            } else {
                orderSortingType = OrderSortingType.ASC;
            }
            String stateParam = ctx.queryParam("state");
            State state = null;
            if (stateParam != null) {
                state = State.valueOf(stateParam);
            }
            reservationList = reservationDao.loadReservationFullDetails(orderSortingType, state);
            setFilterByAndOrderBySelectedValues(ctx, model);
        }

        List<ReservationDto> reservation = reservationDao.loadAllReservations();

        List<RoomWithTypeDto> roomAvailability = roomDao.roomAvailability();


        String strStartDate = ctx.queryParam("startDate");
        String strEndDate = ctx.queryParam("endDate");

        if (strStartDate != null && strEndDate != null) {

            Date startDate = Date.valueOf(strStartDate);
            Date endDate = Date.valueOf(strEndDate);

            System.out.println(startDate);
            System.out.println(endDate);

            List<ReservationFullDetails> searchByDate = reservationList.stream()
                    .filter(d -> d.getStartDate().compareTo(startDate) >= 0 && d.getEndDate().compareTo(endDate) <= 0)
                    .collect(Collectors.toList());


            System.out.println(searchByDate);

            List<Double> totalPriceList = totalPrice();


            model.put("reservationList", searchByDate);
            model.put("roomIdList", roomAvailability);
            model.put("priceReservations", totalPriceList);
            ctx.render("reservation.vm", model);

        } else if (strCustomerId != null) {

            List<Double> totalPriceList = totalPrice();
            int customerId = Integer.parseInt(strCustomerId);

            List<CustomerReservationDto> reservationCustomer = customerReservationDao.getCustomerReservations(customerId);
            model.put("reservationList", reservationCustomer);
            model.put("roomIdList", roomAvailability);
            model.put("priceReservations", totalPriceList);
            ctx.render("reservation.vm", model);

        } else {

            List<Double> totalPriceList = totalPrice();

            model.put("reservationList", reservationList);
            model.put("roomIdList", roomAvailability);
            model.put("priceReservations", totalPriceList);
            ctx.render("reservation.vm", model);

        }
    }


    public List<Double> totalPrice() {

        List<ReservationDto> res = reservationDao.loadAllReservations();
        List<Double> price = new ArrayList<>();

        for (ReservationDto reservation : res) {

            Date startDate = reservation.getStartDate();
            Date endDate = reservation.getEndDate();

            long difference = endDate.getTime() - startDate.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(difference);
            RoomDto room = roomDao.loadById(reservation.getRoomId()).orElseThrow();
            RoomTypeDto roomType = roomTypeDao.loadById(room.getRoomTypeId()).orElseThrow();
            double priceTotal = roomType.getPrice() * days;
            price.add(priceTotal);
        }
        return price;
    }

    private void setSearchByFieldAndSearchTextValues(Context ctx, Map<String, Object> model) {
        String searchText = ctx.queryParam("searchText");
        if (searchText != null) {
            model.put("searchText", searchText);
        }
        String searchField = ctx.queryParam("searchField");
        if (searchField != null) {
            model.put(searchField, true);
        }
    }

    private void setFilterByAndOrderBySelectedValues(Context ctx, Map<String, Object> model) {
        String orderParam = ctx.queryParam("order");
        if (orderParam != null) {
            model.put(orderParam, true);
        } else {
            model.put(OrderSortingType.ASC.getLabel(), true);
        }
        String stateParam = ctx.queryParam("state");
        if (stateParam != null) {
            model.put(stateParam, true);
        } else {
            model.put("ALL", true);
        }
    }

    public static void handleDeleteRequest(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        ReservationDao.deleteReservation(id);
        ctx.redirect("/reservations");
    }

}
