package wantsome.project.ui.web.room;

import io.javalin.http.Context;
import wantsome.project.db.rooms.roomDao.RoomDao;
import wantsome.project.db.rooms.roomDto.RoomDto;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddEditRoomPageController {

    private final RoomDao roomDao;

    public AddEditRoomPageController() {

        roomDao = new RoomDao();
    }


    public void showAddRoomPage(Context ctx) {
        renderAddUpdateForm(ctx, -1, -1, "");

    }

    public void showUpdateRoomPage(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        RoomDto room = roomDao.loadById(id).orElseThrow(() -> new RuntimeException("Room with id " + id + " not found!"));
        renderAddUpdateForm(ctx,
                room.getId(),
                room.getRoomTypeId(),
                room.getExtraInfo()
        );
    }

    /**
     * Render the add or update form. If id>=0, we are on 'update' case (else on 'add')
     */

    private void renderAddUpdateForm(Context ctx, int id, int roomTypeId, String extraInfo) {

        Map<String, Object> model = new HashMap<>();
        model.put("isUpdate", id > 0);
        model.put("prevId", id);
        model.put("prevRoomTypeId", roomTypeId);
        model.put("prevExtraInfo", extraInfo);
        model.put("roomOptions", roomDao.loadAll());
        ctx.render("add_edit_room.vm", model);


    }

    private void validateAndBuildRoom(int roomTypeId, String extraInfo) {
        if (roomTypeId == 0) {
            throw new RuntimeException("ID is required!");
        }

        if (extraInfo == null || extraInfo.isEmpty()) {
            throw new RuntimeException("Extra Info is required!");

        }

        new RoomDto(-1, roomTypeId, extraInfo);
    }


    public void addRoom(Context ctx) {
        int id=Integer.parseInt(Objects.requireNonNull(ctx.formParam("id")));
        int roomTypeId = Integer.parseInt(Objects.requireNonNull(ctx.formParam("roomTypeId")));
        String extraInfo = ctx.formParam("extraInfo");
        validateAndBuildRoom(roomTypeId, extraInfo);
        roomDao.insert(new RoomDto(id, roomTypeId, extraInfo));
        ctx.redirect("/rooms");

    }

    public void updateRoom(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).getOrDefault(-1);
        int roomTypeId = ctx.formParamAsClass("roomTypeId", Integer.class).get();
        String extraInfo = ctx.formParamAsClass("extraInfo", String.class).allowNullable().get();
        roomDao.updateById(id, roomTypeId, extraInfo);
        ctx.redirect("/rooms");
    }


}
