package wantsome.project.ui.web.room;

import io.javalin.http.Context;
import wantsome.project.db.rooms.AvailabilityRoom;
import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.db.rooms.RoomWithTypeDto;
import wantsome.project.db.rooms.roomDao.RoomDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoomPageController {

    private final RoomDao roomDao;


    public RoomPageController() {
        roomDao = new RoomDao();
    }


    public void showRoomPage(Context ctx) {
        RoomTypeEnum roomType = getFilterByRoomType(ctx);

        List<RoomWithTypeDto> roomDto = roomDao.roomFullDetails(roomType);

        Map<String, Object> model = new HashMap<>();


        setAvailableRoom(roomDto);

        model.put("roomList", roomDto);

        setFilterBySelectedValues(ctx, model);

        ctx.render("rooms.vm", model);
    }


    public void setAvailableRoom(List<RoomWithTypeDto> rooms) {

        List<RoomWithTypeDto> roomListAvailability = roomDao.roomAvailability();

        for (RoomWithTypeDto room : rooms) {
            if (roomListAvailability.contains(room)) {
                room.setAvailability(AvailabilityRoom.valueOf("AVAILABLE"));
            } else {
                room.setAvailability(AvailabilityRoom.valueOf("OCCUPIED"));
            }
        }
    }


    private void setFilterBySelectedValues(Context ctx, Map<String, Object> model) {
        String roomType = ctx.queryParam("type");
        if(roomType != null)
        model.put( roomType, true);
    }

    private RoomTypeEnum getFilterByRoomType(Context ctx) {
        String roomType = ctx.queryParam("type");
        return roomType != null ? RoomTypeEnum.parseEnum(roomType) : null;
    }


    public static void handleDeleteRequest(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        RoomDao.deleteRoom(id);
        ctx.redirect("/rooms");
    }

}
