package wantsome.project.ui.web.login;

import io.javalin.http.Context;
import wantsome.project.db.users.UserDao;
import wantsome.project.db.users.UserDto;

import java.util.HashMap;
import java.util.Map;

public class LoginPageController {
    private static final String SESSION_KEY_USER_ID = "userId";
    private static final String SESSION_KEY_EMAIL = "email";
    private static final String SESSION_KEY_PASSWORD = "password";
    private static final String SESSION_KEY_NO_MATCH = "noMatch";



    public static int getSessionKeyUserId(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_USER_ID);
    }

    public static String getSessionKeyEmail(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_EMAIL);
    }

    public static String getSessionKeyPassword(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_PASSWORD);
    }

    public static boolean getSessionKeyNoMatch(Context ctx) {
        return ctx.sessionAttribute("noMatch") != null ? ctx.sessionAttribute(SESSION_KEY_NO_MATCH) : false;
    }

    public static void setSessionKeyUserId(Context ctx, int id) {
        ctx.sessionAttribute(SESSION_KEY_USER_ID, id);
    }

    public static void setSessionKeyEmail(Context ctx, String email) {
        ctx.sessionAttribute(SESSION_KEY_EMAIL, email);
    }

    public static void setSessionKeyPassword(Context ctx, String password) {
        ctx.sessionAttribute(SESSION_KEY_EMAIL, password);
    }

    public static void setSessionKeyMatch(Context ctx, boolean noMatch) {
        ctx.sessionAttribute(SESSION_KEY_NO_MATCH, noMatch);
    }

    public static void signUpFormPage(Context ctx) {
        UserDao userDao = new UserDao();
        String firstName = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        userDao.insert(new UserDto(firstName, lastName, email, password));
        ctx.redirect("/");
    }

    public static void logInValidate(Context ctx) {
        UserDao userDao = new UserDao();
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        int id = userDao.getIDByEmailAndPassword(email, password);
        setSessionKeyUserId(ctx, id);
        if (id == -1) {
            setSessionKeyMatch(ctx, true);
            ctx.redirect("/");
        } else {
            setSessionKeyEmail(ctx, email);
            ctx.sessionAttribute("userEmail", email);
            setSessionKeyPassword(ctx, password);
            ctx.redirect("/main");
        }
    }

    public static void logInPage(Context ctx) {
        boolean noMatch = LoginPageController.getSessionKeyNoMatch(ctx);
        Map<String, Object> model = new HashMap<>();
        model.put("noMatch", noMatch);
        ctx.render("login.vm", model);
    }

    private static boolean isRestrictedPage(String path) {
        return path.startsWith("/main")
                || path.startsWith("/reservations")
                || path.startsWith("/customers")
                || path.startsWith("/rooms");
    }
    public static void checkUserIsLoggedIn(Context ctx) {
        if (isRestrictedPage(ctx.path())
                && ctx.sessionAttribute("userEmail") == null) {
            ctx.redirect("/");
        }
    }



}
