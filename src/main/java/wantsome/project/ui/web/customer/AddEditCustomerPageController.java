package wantsome.project.ui.web.customer;

import io.javalin.http.Context;
import wantsome.project.db.customers.customerDao.CustomerDao;
import wantsome.project.db.customers.customerDto.CustomerDto;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AddEditCustomerPageController {


    private final CustomerDao customerDao;

    public AddEditCustomerPageController() {
        customerDao = new CustomerDao();
    }

    public void showAddCustomerPage(Context ctx) {
        renderAddUpdateForm(ctx, -1, "", "", "", "");

    }


    public void showUpdateCustomerPage(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        Optional<CustomerDto> customerOpt = customerDao.loadById(id);
        CustomerDto customer = customerOpt.orElseThrow(() -> new RuntimeException("Customer with id " + id + " not found!"));
        renderAddUpdateForm(ctx,
                customer.getId(),
                customer.getFirst_name(),
                customer.getLast_name(),
                customer.getEmail(),
                customer.getAddress());
    }

    /**
     * Render the add or update form. If id>=0, we are on 'update' case (else on 'add')
     */

    private void renderAddUpdateForm(Context ctx, int id, String firstName, String lastName, String email, String address) {
        Map<String, Object> model = new HashMap<>();

        model.put("isUpdate", id >= 0);

        model.put("prevId", id);
        model.put("prevFirstName", firstName);
        model.put("prevLastName", lastName);
        model.put("prevEmail", email);
        model.put("prevAddress", address);


        model.put("customerOptions", customerDao.loadAll());


        ctx.render("add_edit_customer.vm", model);
    }


    private void validateAndBuildCustomer(String firstName, String lastName, String email, String address) {

        if (firstName == null || firstName.isEmpty()) {
            throw new RuntimeException("First name is required!");
        }
        if (lastName == null || lastName.isEmpty()) {
            throw new RuntimeException("Last name is required!");
        }

        if (email == null || email.isEmpty()) {
            throw new RuntimeException("Email is required!");
        }
        if (address == null || address.isEmpty()) {
            throw new RuntimeException("Address is required!");
        }

        new CustomerDto(-1, firstName, lastName, email, address);
    }


    public void addCustomer(Context ctx) {
        String firstName = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String email = ctx.formParam("email");
        String address = ctx.formParam("address");

        validateAndBuildCustomer(firstName, lastName, email, address);
        customerDao.insert(new CustomerDto(-1, firstName, lastName, email, address));
        ctx.redirect("/customers");

    }


    public void updateCustomer(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).getOrDefault(-1);
        String firstName = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String email = ctx.formParam("email");
        String address = ctx.formParam("address");

        customerDao.updateById(id, firstName, lastName, email, address);
        ctx.redirect("/customers");
    }
}
