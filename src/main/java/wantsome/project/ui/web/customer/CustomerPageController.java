package wantsome.project.ui.web.customer;

import io.javalin.http.Context;
import wantsome.project.db.customers.SearchByField;
import wantsome.project.db.customers.customerDao.CustomerDao;
import wantsome.project.db.customers.customerDto.CustomerDto;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CustomerPageController {

    private final CustomerDao customerDao;

    public CustomerPageController() {
        customerDao = new CustomerDao();
    }

    public void showCustomerPage(Context ctx) {

        Map<String, Object> model = new LinkedHashMap<>();
        String searchText = ctx.queryParam("searchText");
        String searchField = ctx.queryParam("searchField");

        List<CustomerDto> customerList = customerDao.loadAll();

        if (searchText != null && searchField != null) {
            SearchByField searchByField = SearchByField.valueOf(searchField);
            customerList = customerDao.search(searchByField, searchText);
            setSearchByFieldAndSearchTextValues(ctx, model);
        }

        model.put("customerList", customerList);
        ctx.render("customers.vm", model);
    }


    private void setSearchByFieldAndSearchTextValues(Context ctx, Map<String, Object> model) {
        String searchText = ctx.queryParam("searchText");
        if (searchText != null) {
            model.put("searchText", searchText);
        }
        String searchField = ctx.queryParam("searchField");
        if (searchField != null) {
            model.put(searchField, true);
        }
    }


    public static void handleDeleteRequest(Context ctx) {
        int id = ctx.pathParamAsClass("id", Integer.class).get();
        CustomerDao.delete(id);
        ctx.redirect("/customers");
    }

}

