package wantsome.project.ui;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.staticfiles.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wantsome.project.other.ConnectionPool;
import wantsome.project.other.DbTable;
import wantsome.project.ui.web.customer.AddEditCustomerPageController;
import wantsome.project.ui.web.customer.CustomerPageController;
import wantsome.project.ui.web.login.LoginPageController;
import wantsome.project.ui.web.reservation.AddEditReservationPageController;
import wantsome.project.ui.web.reservation.ReservationPageController;
import wantsome.project.ui.web.room.AddEditRoomPageController;
import wantsome.project.ui.web.room.RoomPageController;

import java.util.HashMap;
import java.util.Map;

/***
 * web application (using Javalin framework)
 */
public class ReservationsWebApp {


    private static final Logger logger = LoggerFactory.getLogger(ReservationsWebApp.class);


    public static void main(String[] args) {

        setup();
        startWebServer();

    }

    private static void setup() {

        ConnectionPool.init();
        DbTable.initDatabase();
    }

    private static void startWebServer() {
        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH);
            config.enableDevLogging();
        }).start();



        //configure all routes for room
        app.get("/main", ReservationsWebApp::getMainWebPage);
        logger.info("Server started: http://localhost:" + app.port());

        //--- LOGIN ---//

        app.before(LoginPageController::checkUserIsLoggedIn);
        app.get("/", LoginPageController::logInPage);
        app.post("/", LoginPageController::logInValidate);
        app.get("/signup", ctx -> ctx.render("signup.vm"));
        app.post("/signup", LoginPageController::signUpFormPage);

        //--- CUSTOMERS---//

        app.get("/customers", ReservationsWebApp::showCustomerPage);
        app.get("/customers/update/{id}", ReservationsWebApp::showUpdateCustomerPage);
        app.post("/customers/update/{id}", ReservationsWebApp::updateCustomer);
        app.get("/customers/add", ReservationsWebApp::showAddCustomer);
        app.post("/customers/add", ReservationsWebApp::addCustomer);
        app.get("/customers/delete/{id}", CustomerPageController::handleDeleteRequest);


        //---RESERVATIONS---//


        app.get("/reservations", ReservationsWebApp::showReservationPage);
        app.get("/reservations/update/{id}", ReservationsWebApp::showUpdateReservationPage);
        app.post("/reservations/update/{id}", ReservationsWebApp::updateReservation);
        app.get("/reservations/delete/{id}", ReservationPageController::handleDeleteRequest);
        app.get("/reservations/add/{customerId}", ReservationsWebApp::showAddReservationPage);
        app.post("/reservations/add/{customerId}", ReservationsWebApp::addReservation);

        //---ROOMS---//

        app.get("/rooms", ReservationsWebApp::showRoomPage);
        app.get("/rooms/update/{id}", ReservationsWebApp::showUpdateRoomPage);
        app.post("/rooms/update/{id}", ReservationsWebApp::updateRoom);
        app.get("/rooms/add", ReservationsWebApp::showAddRoomPage);
        app.post("/rooms/add", ReservationsWebApp::addRoomPage);
        app.get("/rooms/delete/{id}", RoomPageController::handleDeleteRequest);


        //see all error
        app.exception(Exception.class, (e, ctx) -> {
            logger.error("Unexpected exception", e);
            ctx.html("Unexpected exception: " + e);
        });


    }

    //Returning web page

    private static void getMainWebPage(Context ctx) {
        Map<String, Object> model = new HashMap<>();
        ctx.render("main.vm", model);
    }


    //Customer
    public static void showCustomerPage(Context ctx) {
        CustomerPageController customerPageController = new CustomerPageController();
        customerPageController.showCustomerPage(ctx);
    }

    public static void updateCustomer(Context ctx) {
        AddEditCustomerPageController addEditCustomerPageController = new AddEditCustomerPageController();
        addEditCustomerPageController.updateCustomer(ctx);
    }

    public static void showUpdateCustomerPage(Context ctx) {
        AddEditCustomerPageController addEditCustomerPageController = new AddEditCustomerPageController();
        addEditCustomerPageController.showUpdateCustomerPage(ctx);
    }

    public static void addCustomer(Context ctx) {
        AddEditCustomerPageController addEditCustomerPageController = new AddEditCustomerPageController();
        addEditCustomerPageController.addCustomer(ctx);
    }

    public static void showAddCustomer(Context ctx) {
        AddEditCustomerPageController addEditCustomerPageController = new AddEditCustomerPageController();
        addEditCustomerPageController.showAddCustomerPage(ctx);
    }


    //Reservations

    public static void showReservationPage(Context ctx) {
        ReservationPageController reservationPageController = new ReservationPageController();
        reservationPageController.showReservationPage(ctx);
    }


    public static void updateReservation(Context ctx) {
        AddEditReservationPageController addEditReservationPageController = new AddEditReservationPageController();
        addEditReservationPageController.updateReservation(ctx);
    }

    public static void showUpdateReservationPage(Context ctx) {
        AddEditReservationPageController addEditReservationPageController = new AddEditReservationPageController();
        addEditReservationPageController.showUpdateReservationPage(ctx);
    }

    public static void showAddReservationPage(Context ctx) {
        AddEditReservationPageController addEditReservationPageController = new AddEditReservationPageController();
        addEditReservationPageController.showAddReservationPage(ctx);

    }

    public static void addReservation(Context ctx) {
        AddEditReservationPageController addEditReservationPageController = new AddEditReservationPageController();
        addEditReservationPageController.addReservation(ctx);

    }

    //Rooms

    private static void showRoomPage(Context ctx) {
        RoomPageController roomPageController = new RoomPageController();
        roomPageController.showRoomPage(ctx);
    }

    public static void showUpdateRoomPage(Context ctx) {
        AddEditRoomPageController addEditRoomPageController = new AddEditRoomPageController();
        addEditRoomPageController.showUpdateRoomPage(ctx);
    }

    public static void updateRoom(Context ctx) {

        AddEditRoomPageController addEditRoomPageController = new AddEditRoomPageController();
        addEditRoomPageController.updateRoom(ctx);
    }


    public static void showAddRoomPage(Context ctx) {

        AddEditRoomPageController addEditRoomPageController = new AddEditRoomPageController();
        addEditRoomPageController.showAddRoomPage(ctx);
    }

    public static void addRoomPage(Context ctx) {

        AddEditRoomPageController addEditRoomPageController = new AddEditRoomPageController();
        addEditRoomPageController.addRoom(ctx);
    }


}
