package wantsome.project.other;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {
    private static BasicDataSource dataSource;

    public static void init() {
        dataSource = new BasicDataSource();
        dataSource.setInitialSize(10);
        dataSource.setDriverClassName("org.sqlite.JDBC");
        dataSource.setUrl("jdbc:sqlite:Reservations.db");
        dataSource.setConnectionProperties("foreign_keys=true;date_class=text;date_string_format=yyyy-MM-dd;");
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
