package wantsome.project.other;

import wantsome.project.db.customers.customerDao.CustomerDao;
import wantsome.project.db.customers.customerDto.CustomerDto;
import wantsome.project.db.reservations.reservationDao.ReservationDao;
import wantsome.project.db.reservations.reservationDto.ReservationDto;
import wantsome.project.db.rooms.roomDao.RoomDao;
import wantsome.project.db.rooms.roomDao.RoomTypeDao;
import wantsome.project.db.rooms.roomDto.RoomDto;
import wantsome.project.db.rooms.roomDto.RoomTypeDto;
import wantsome.project.db.users.UserDao;
import wantsome.project.db.users.UserDto;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.reservations.State.*;
import static wantsome.project.db.rooms.RoomTypeEnum.*;

public class DbTable {

    private static final String CREATE_USER_SQL=
            "CREATE TABLE IF NOT EXISTS USERS(\n" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "FIRST_NAME TEXT NOT NULL,\n" +
                    "LAST_NAME TEXT NOT NULL,\n" +
                    "EMAIL TEXT NOT NULL UNIQUE,\n" +
                    "PASSWORD TEXT NOT NULL\n" +
                    ");";

    private static final String CREATE_CUSTOMER_SQL =
            "CREATE TABLE IF NOT EXISTS CUSTOMER(" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "FIRST_NAME TEXT NOT NULL," +
                    "LAST_NAME TEXT NOT NULL," +
                    "EMAIL TEXT NOT NULL UNIQUE," +
                    "ADDRESS TEXT NOT NULL" +
                    ");";


    private static final String CREATE_RESERVATIONS_SQL =
            "CREATE TABLE IF NOT EXISTS RESERVATION(" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "CUSTOMER_ID INTEGER REFERENCES CUSTOMER(ID) NOT NULL," +
                    "START_DATE DATE NOT NULL," +
                    "END_DATE DATE NOT NULL," +
                    "ROOM_ID INTEGER REFERENCES ROOM(ID) NOT NULL," +
                    "EXTRA_INFO TEXT," +
                    "STATE TEXT CHECK (STATE IN('" + PAID + "', '" + CANCELED + "','" + ACTIVE + "')) NOT NULL" +
                    ");";


    private static final String CREATE_ROOM_SQL =
            "CREATE TABLE IF NOT EXISTS ROOM(" +
                    "ID INTEGER PRIMARY KEY," +
                    "ROOM_TYPE_ID INTEGER REFERENCES ROOM_TYPE(ID) NOT NULL," +
                    "EXTRA_INFO TEXT NOT NULL" +
                    ");";


    private static final String CREATE_ROOM_TYPE_SQL =
            "CREATE TABLE IF NOT EXISTS ROOM_TYPE(" +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "DESCRIPTION TEXT CHECK (DESCRIPTION IN('" + SINGLE + "', '" + DOUBLE + "','" + TRIPLE + "', '" + PENTHOUSE + "', '" + PRESIDENTIAL + "')) NOT NULL," +
                    "PRICE REAL NOT NULL," +
                    "CAPACITY INTEGER NOT NULL" +
                    ");";


    public static void initDatabase() {
        createMissingTables();
        sampleUsersList();
        sampleRoomTypesList();
        sampleRoomList();
        sampleCustomerList();
        sampleReservationList();


    }

    private static void createMissingTables() {

        ConnectionPool.init();

        try (Connection connection = ConnectionPool.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(CREATE_USER_SQL);
            statement.execute(CREATE_CUSTOMER_SQL);
            statement.execute(CREATE_RESERVATIONS_SQL);
            statement.execute(CREATE_ROOM_SQL);
            statement.execute(CREATE_ROOM_TYPE_SQL);

        } catch (SQLException e) {

            throw new RuntimeException("Error creating missing tables: " + e.getMessage());
        }
    }



    /***
     * Same dates for Users list
     */
    private static void sampleUsersList(){
        UserDao userDao=new UserDao();
        if(userDao.getAll().isEmpty()){
            userDao.insert(new UserDto(1, "Ilinka", "Prisacaru", "ilinca@gmail.com", "prisacaru"));
        }
    }


    /***
     * Same dates for Customer list
     */

    private static void sampleCustomerList() {

        CustomerDao customerDao = new CustomerDao();
        if (customerDao.loadAll().isEmpty()) {

            customerDao.insert(new CustomerDto(1, "Aaron", "Hank", "aron_hank@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(2, "Oliver", "Mason", "oliver_mason@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(3, "Jake", "Liam", "jake_liam@gmail.com", "Antalia, Turcia"));
            customerDao.insert(new CustomerDto(4, "Connor", "Rodriguez", "connor_rod@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(5, "Callum", "Smith", "callum_smith@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(6, "Jacob", "Johnson", "jacob_johnson@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(7, "Kyle", "Brown", "kyle_brown@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(8, "William", "Walsh", "william_walsh@gmail.com", "Pacific, USA"));
            customerDao.insert(new CustomerDto(9, "Noah", "Taylor", "noah_taylor@gmail.com", "Washington,USA"));
            customerDao.insert(new CustomerDto(10, "Michael", "Jones", "michael_jones@gmail.com", "Hawaii, USA"));
            customerDao.insert(new CustomerDto(11, "Alexander", "Smith", "alexa_smith@gmail.com", "Tokio, Japonia"));
            customerDao.insert(new CustomerDto(12, "James", "Murphy", "james_murphy@gmail.com", "Maryland, USA"));
            customerDao.insert(new CustomerDto(13, "Daniel", "O'Neill", "daniel_neil@gmail.com", "Dublin, Ireland"));
            customerDao.insert(new CustomerDto(14, "George", "O'Connor", "george_connor@gmail.com", "Roma, Italy"));
            customerDao.insert(new CustomerDto(15, "Oscar", "Evans", "oscar_evans@gmail.com", "Napoli, Italy"));
            customerDao.insert(new CustomerDto(16, "Otis", "Wilson", "oris_wilson@gmail.com", "Bucuresti, Romania"));
            customerDao.insert(new CustomerDto(17, "Sergiu", "Thomas", "sergiu_thomas@gmail.com", "Suceava, Romania"));
            customerDao.insert(new CustomerDto(18, "Ionela", "Garcia", "ionela_garcia@gmail.com", "Virginia, USA"));
            customerDao.insert(new CustomerDto(19, "Harry", "O'Kelly", "harry_kelly@gmail.com", "Florida, USA"));
            customerDao.insert(new CustomerDto(20, "Florin", "Lam", "florin_lam@gmail.com", "Botosani, Romania"));
            customerDao.insert(new CustomerDto(21, "Marian", "Morton", "marian_morton@gmail.com", "Oregon"));
            customerDao.insert(new CustomerDto(22, "Isabella", "Joanne", "isabella_joanne@gmail.com", "Targu_Neamt, Romania"));
            customerDao.insert(new CustomerDto(23, "Sophia", "Smith", "sophia_smith@gmail.com", "Austin,Texas"));
            customerDao.insert(new CustomerDto(24, "Emma", "Byrne", "emma_byrne@gmail.com", "Suceava, Romania"));
            customerDao.insert(new CustomerDto(25, "Margaret", "Ava", "margaret_ava@gmail.com", "Indiana, USA"));
            customerDao.insert(new CustomerDto(26, "Amelia", "Poppy", "amelia_poppy@gmail.com", "Georgia, USA"));
            customerDao.insert(new CustomerDto(27, "Lily", "Singh", "lily_s@gmail.com", "Alaska, USA"));
            customerDao.insert(new CustomerDto(28, "Tracy", "Li", "tracy_li@gmail.com", "Hayastan, Armenia"));
            customerDao.insert(new CustomerDto(29, "Charlotte", "Barbara", "charlotte_barb@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(30, "Madalina", "Prisacaru", "madalina_prisacaru@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(31, "Traian", "Chirila", "traian_chirila@gmail.com", "Iasi, Romania"));
            customerDao.insert(new CustomerDto(32, "Carol", "Roy", "carol_roy@gmail.com", "California, USA"));
            customerDao.insert(new CustomerDto(33, "Alexandru", "Engelbert", "alexandru_engelbert@gmail.com", "Cracovia, Polonia"));
            customerDao.insert(new CustomerDto(34, "Wei", "Wang", "wei_wang@gmail.com", "Tokio, Japonia"));
            customerDao.insert(new CustomerDto(35, "Levi", "Mason", "levi_mason@gmail.com", "Bucuresti, Romania"));
            customerDao.insert(new CustomerDto(36, "Ezra", "Luca", "ezra_luca@gmail.com", "Brasov, Romania"));
        }
    }



    /***
     * All types of room
     */

    private static void sampleRoomTypesList() {

        if (RoomTypeDao.loadAll().isEmpty()) {

            RoomTypeDao.insert(new RoomTypeDto(1, SINGLE, 200, 1));
            RoomTypeDao.insert(new RoomTypeDto(2, DOUBLE, 400, 2));
            RoomTypeDao.insert(new RoomTypeDto(3, TRIPLE, 550, 3));
            RoomTypeDao.insert(new RoomTypeDto(4, PENTHOUSE, 800, 4));
            RoomTypeDao.insert(new RoomTypeDto(5, PRESIDENTIAL, 1200, 5));


        }
    }


    /***
     * Same dates for Room list
     */


    private static void sampleRoomList() {

        RoomDao roomDao = new RoomDao();

        if (roomDao.loadAll().isEmpty()) {

            roomDao.insert(new RoomDto(101, 1, "First floor"));
            roomDao.insert(new RoomDto(102, 1, "First floor"));
            roomDao.insert(new RoomDto(103, 1, "First floor"));
            roomDao.insert(new RoomDto(104, 2, "First floor"));
            roomDao.insert(new RoomDto(105, 2, "First floor"));
            roomDao.insert(new RoomDto(106, 2, "First floor"));
            roomDao.insert(new RoomDto(107, 2, "First floor"));
            roomDao.insert(new RoomDto(108, 3, "First floor"));
            roomDao.insert(new RoomDto(109, 3, "First floor"));
            roomDao.insert(new RoomDto(110, 4, "First floor"));
            roomDao.insert(new RoomDto(111, 4, "First floor"));
            roomDao.insert(new RoomDto(112, 5, "First floor"));
            roomDao.insert(new RoomDto(201, 1, "Second floor"));
            roomDao.insert(new RoomDto(202, 1, "Second floor"));
            roomDao.insert(new RoomDto(203, 1, "Second floor"));
            roomDao.insert(new RoomDto(204, 2, "Second floor"));
            roomDao.insert(new RoomDto(205, 2, "Second floor"));
            roomDao.insert(new RoomDto(206, 2, "Second floor"));
            roomDao.insert(new RoomDto(207, 3, "Second floor"));
            roomDao.insert(new RoomDto(208, 3, "Second floor"));
            roomDao.insert(new RoomDto(209, 3, "Second floor"));
            roomDao.insert(new RoomDto(210, 4, "Second floor"));
            roomDao.insert(new RoomDto(211, 4, "Second floor"));
            roomDao.insert(new RoomDto(212, 5, "Second floor"));
            roomDao.insert(new RoomDto(301, 1, "Third floor"));
            roomDao.insert(new RoomDto(302, 1, "Third floor"));
            roomDao.insert(new RoomDto(303, 1, "Third floor"));
            roomDao.insert(new RoomDto(304, 2, "Third floor"));
            roomDao.insert(new RoomDto(305, 2, "Third floor"));
            roomDao.insert(new RoomDto(306, 3, "Third floor"));
            roomDao.insert(new RoomDto(307, 3, "Third floor"));
            roomDao.insert(new RoomDto(308, 3, "Third floor"));
            roomDao.insert(new RoomDto(309, 4, "Third floor"));
            roomDao.insert(new RoomDto(310, 4, "Third floor"));
            roomDao.insert(new RoomDto(311, 4, "Third floor"));
            roomDao.insert(new RoomDto(312, 5, "Third floor"));


        }
    }

    /***
     * Same dates for Reservation list
     */

    private static void sampleReservationList() {
        ReservationDao reservationDao = new ReservationDao();
        if (reservationDao.loadAll(null, null).isEmpty()) {
            reservationDao.insertReservation(new ReservationDto(1, 1, Date.valueOf("2022-06-09"), Date.valueOf("2022-06-12"), 101, "", PAID));
            reservationDao.insertReservation(new ReservationDto(2, 2, Date.valueOf("2022-06-03"), Date.valueOf("2022-06-04"), 102, "", PAID));
            reservationDao.insertReservation(new ReservationDto(3, 3, Date.valueOf("2022-06-05"), Date.valueOf("2022-06-07"), 103, "", PAID));
            reservationDao.insertReservation(new ReservationDto(4, 4, Date.valueOf("2022-02-02"), Date.valueOf("2022-02-03"), 104, "", PAID));
            reservationDao.insertReservation(new ReservationDto(5, 5, Date.valueOf("2022-03-01"), Date.valueOf("2022-03-02"), 105, "", PAID));
            reservationDao.insertReservation(new ReservationDto(6, 6, Date.valueOf("2022-03-03"), Date.valueOf("2022-03-04"), 106, "", PAID));
            reservationDao.insertReservation(new ReservationDto(7, 7, Date.valueOf("2022-03-05"), Date.valueOf("2022-03-08"), 107, "", PAID));
            reservationDao.insertReservation(new ReservationDto(8, 8, Date.valueOf("2022-03-10"), Date.valueOf("2022-03-12"), 108, "", PAID));
            reservationDao.insertReservation(new ReservationDto(9, 9, Date.valueOf("2022-03-16"), Date.valueOf("2022-03-23"), 109, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(10, 10, Date.valueOf("2022-04-01"), Date.valueOf("2022-04-02"), 110, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(11, 11, Date.valueOf("2022-04-03"), Date.valueOf("2022-04-04"), 111, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(12, 12, Date.valueOf("2022-04-05"), Date.valueOf("2022-04-06"), 112, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(13, 13, Date.valueOf("2022-04-08"), Date.valueOf("2022-04-10"), 201, "", PAID));
            reservationDao.insertReservation(new ReservationDto(14, 14, Date.valueOf("2022-04-11"), Date.valueOf("2022-04-14"), 202, "", PAID));
            reservationDao.insertReservation(new ReservationDto(15, 15, Date.valueOf("2022-04-16"), Date.valueOf("2022-04-20"), 203, "", PAID));
            reservationDao.insertReservation(new ReservationDto(16, 16, Date.valueOf("2022-04-20"), Date.valueOf("2022-04-22"), 204, "", PAID));
            reservationDao.insertReservation(new ReservationDto(17, 17, Date.valueOf("2022-04-23"), Date.valueOf("2022-04-24"), 205, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(18, 18, Date.valueOf("2022-04-25"), Date.valueOf("2022-04-26"), 206, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(19, 19, Date.valueOf("2022-04-28"), Date.valueOf("2022-04-29"), 207, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(20, 20, Date.valueOf("2022-05-01"), Date.valueOf("2022-05-02"), 208, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(21, 21, Date.valueOf("2022-05-03"), Date.valueOf("2022-05-04"), 209, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(22, 22, Date.valueOf("2022-05-05"), Date.valueOf("2022-05-06"), 210, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(23, 23, Date.valueOf("2022-05-07"), Date.valueOf("2022-05-08"), 211, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(24, 24, Date.valueOf("2022-05-09"), Date.valueOf("2022-05-10"), 212, "", CANCELED));
            reservationDao.insertReservation(new ReservationDto(25, 25, Date.valueOf("2022-05-11"), Date.valueOf("2022-05-12"), 301, "", PAID));
            reservationDao.insertReservation(new ReservationDto(26, 26, Date.valueOf("2022-07-05"), Date.valueOf("2022-07-08"), 302, "", PAID));
            reservationDao.insertReservation(new ReservationDto(27, 27, Date.valueOf("2022-07-09"), Date.valueOf("2022-07-10"), 303, "", PAID));
            reservationDao.insertReservation(new ReservationDto(28, 28, Date.valueOf("2022-07-11"), Date.valueOf("2022-07-12"), 304, "", PAID));
            reservationDao.insertReservation(new ReservationDto(29, 29, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-20"), 305, "", ACTIVE));
            reservationDao.insertReservation(new ReservationDto(30, 30, Date.valueOf("2022-08-21"), Date.valueOf("2022-08-22"), 306, "", ACTIVE));
        }
    }

}
