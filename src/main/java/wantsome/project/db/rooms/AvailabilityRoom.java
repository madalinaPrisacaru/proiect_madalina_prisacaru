package wantsome.project.db.rooms;

public enum AvailabilityRoom{
    OCCUPIED,
    AVAILABLE
}
