package wantsome.project.db.rooms.roomDao;

import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.db.rooms.roomDto.RoomTypeDto;
import wantsome.project.other.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RoomTypeDao{

    /**
     * Load list of Type Room from DB (all found)
     */

    public static List<RoomTypeDto> loadAll() {

        String sql = "SELECT*FROM ROOM_TYPE;";

        List<RoomTypeDto> roomTypeList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    roomTypeList.add(extractRoomTypeFromResult(resultSet));
                }
            }

            return roomTypeList;

        } catch (SQLException e) {

            throw new RuntimeException("Error loading room type: " + e.getMessage());
        }


    }

    /**
     * Auxiliary method  for room type
     */

    private static RoomTypeDto extractRoomTypeFromResult(ResultSet resultSet) throws SQLException {
        return new RoomTypeDto(
                resultSet.getInt("id"),
                RoomTypeEnum.valueOf(resultSet.getString("description")),
                resultSet.getDouble("price"),
                resultSet.getInt("capacity"));

    }


    /**
     * Load one specific roomType_id from DB (by id)
     */

    public Optional<RoomTypeDto> loadById(int id) {

        String sql = "SELECT * FROM ROOM_TYPE WHERE  ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    RoomTypeDto room = extractRoomTypeFromResult(resultSet);
                    return Optional.of(room);
                }

                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error loading room_type by id " + id + ": " + e.getMessage());
        }


    }


    /***
     * Insert Room_Type
     */

    public static void insert(RoomTypeDto room) {

        String sql = "INSERT INTO ROOM_TYPE(ID,DESCRIPTION,PRICE,CAPACITY) VALUES(?,?,?,?);";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, room.getId());
            statement.setObject(2, room.getDescription());
            statement.setDouble(3, room.getPrice());
            statement.setInt(4, room.getCapacity());
            statement.execute();


        } catch (SQLException e) {

            throw new RuntimeException("Error saving new room type: " + e.getMessage());
        }
    }
}
