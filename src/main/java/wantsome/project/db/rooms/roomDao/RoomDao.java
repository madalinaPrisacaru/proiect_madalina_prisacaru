package wantsome.project.db.rooms.roomDao;

import wantsome.project.db.AbstractDao;
import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.db.rooms.RoomWithTypeDto;
import wantsome.project.db.rooms.roomDto.RoomDto;
import wantsome.project.other.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RoomDao extends AbstractDao{

    /***
     * Load list of room from DB (all found) but nicely
     */

    public List<RoomWithTypeDto> roomFullDetails(RoomTypeEnum roomType) {

        String sql = "SELECT r.ID, r.ROOM_TYPE_ID, r.EXTRA_INFO, rt.DESCRIPTION, rt.PRICE, rt.CAPACITY " +
                "FROM ROOM r JOIN ROOM_TYPE rt ON r.ROOM_TYPE_ID = rt.ID ";

        String filterBySql =getFilterByQuery(roomType);

        List<RoomWithTypeDto> roomList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql + filterBySql)) {

            setFilterByParameters(statement, roomType);

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    roomList.add(new RoomWithTypeDto(
                            resultSet.getInt("ID"),
                            resultSet.getInt("ROOM_TYPE_ID"),
                            resultSet.getString("EXTRA_INFO"),
                            resultSet.getString("DESCRIPTION"),
                            resultSet.getDouble("price"),
                            resultSet.getInt("capacity")));

                }
            }

            return roomList;
        } catch (SQLException e) {

            throw new RuntimeException("Error loading room type: " + e.getMessage());
        }
    }


    private void setFilterByParameters(PreparedStatement statement, RoomTypeEnum roomType) throws SQLException {
        int index = 1;
        if (roomType != null) {
            statement.setString(index, roomType.getLabel());
        }
    }

    private String getFilterByQuery(RoomTypeEnum roomType) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (roomType != null) {
            this.addFilter(stringBuilder, 1, "rt.DESCRIPTION");
        }
        return stringBuilder.toString();
    }

    /**
     * Load list of room from DB (all found)
     */

    public List<RoomDto> loadAll() {

        String sql = "SELECT * FROM ROOM;";


        List<RoomDto> roomList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            try (ResultSet resultSet = statement.executeQuery()) {


                while (resultSet.next()) {
                    roomList.add(extractRoomFromResult(resultSet));

                }
            }

            return roomList;

        } catch (SQLException e) {

            throw new RuntimeException("Error loading room type: " + e.getMessage());
        }
    }


    /**
     * Load one specific room from DB (by id)
     */

    public Optional<RoomDto> loadById(int id) {
        String sql = "SELECT * FROM ROOM WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    RoomDto roomDto = extractRoomFromResult(resultSet);
                    return Optional.of(roomDto);
                }

                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error loading room by id " + id + ": " + e.getMessage());
        }

    }


    /***
     * Add a new room to DB
     */

    public void insert(RoomDto room) {

        String sql = "INSERT INTO ROOM(ID, ROOM_TYPE_ID, EXTRA_INFO) VALUES(?,?,?);";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, room.getId());
            statement.setInt(2, room.getRoomTypeId());
            statement.setString(3, room.getExtraInfo());
            statement.execute();


        } catch (SQLException e) {

            throw new RuntimeException("Error saving new room: " + e.getMessage());
        }
    }

    /**
     * Update an existing room to DB
     */

    public void updateById(int id, int roomTypeId, String extraInfo) {

        String sql = "UPDATE  ROOM SET ROOM_TYPE_ID=?, " +
                " EXTRA_INFO=?  " +
                " WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, roomTypeId);
            statement.setString(2, extraInfo);
            statement.setInt(3, id);

            statement.executeUpdate();


        } catch (SQLException e) {

            System.err.println("Error updating an existing room: " + roomTypeId + " " + extraInfo + " " + id + " " + e.getMessage());

        }

    }


    /**
     * Delete a room from DB
     */

    public static void deleteRoom(int id) {

        String sql = " DELETE FROM ROOM WHERE ROOM.ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {

            System.err.println("Error delete room by id: " + id);

        }

    }

    /**
     * Auxiliary method  for room
     */

    private static RoomDto extractRoomFromResult(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        int roomTypeId = rs.getInt("room_type_id");
        String extraInfo = rs.getString("extra_info");
        return new RoomDto(id, roomTypeId, extraInfo);
    }

    public List<RoomWithTypeDto> roomAvailability() {

        String sql = "SELECT r.ID, r.ROOM_TYPE_ID, r.EXTRA_INFO, rt.DESCRIPTION, rt.PRICE, rt.CAPACITY \n" +
                "FROM ROOM r JOIN ROOM_TYPE rt ON r.ROOM_TYPE_ID = rt.ID WHERE NOT EXISTS " +
                " (SELECT * FROM RESERVATION r2 WHERE r2.ROOM_ID=r.ID);";

        List<RoomWithTypeDto> roomList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();

             PreparedStatement statement = connection.prepareStatement(sql)) {

            try (ResultSet resultSet = statement.executeQuery()) {


                while (resultSet.next()) {
                    roomList.add(new RoomWithTypeDto(
                            resultSet.getInt("ID"),
                            resultSet.getInt("ROOM_TYPE_ID"),
                            resultSet.getString("EXTRA_INFO"),
                            resultSet.getString("DESCRIPTION"),
                            resultSet.getDouble("price"),
                            resultSet.getInt("capacity")));

                }
            }

            return roomList;

        } catch (SQLException e) {

            throw new RuntimeException("Error loading availability room: " + e.getMessage());
        }


    }


}

