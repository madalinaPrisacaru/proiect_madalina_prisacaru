package wantsome.project.db.rooms.roomDto;

import wantsome.project.db.rooms.RoomTypeEnum;

import java.util.Objects;

public class RoomTypeDto{


    private final int id;
    private final RoomTypeEnum description;
    private double price;
    private final int capacity;


    public RoomTypeDto(int id, RoomTypeEnum description,
                       double price, int capacity) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.capacity = capacity;
        typePrice();
    }


    public int getId() {
        return id;
    }

    public RoomTypeEnum getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getCapacity() {
        return capacity;
    }


    @Override
    public String toString() {
        return "Room_Types{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", capacity=" + capacity +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomTypeDto)) return false;
        RoomTypeDto that = (RoomTypeDto) o;
        return getId() == that.getId() && Double.compare(that.getPrice(), getPrice()) == 0
                && getCapacity() == that.getCapacity()
                && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getPrice(), getCapacity());
    }


    public void typePrice() {

        if (description == RoomTypeEnum.SINGLE) {
            this.price = 200;
        }

        if (description == RoomTypeEnum.DOUBLE) {
            this.price = 400;
        }

        if (description == RoomTypeEnum.TRIPLE) {
            this.price = 550;
        }
        if (description == RoomTypeEnum.PENTHOUSE) {
            this.price = 800;
        }

        if (description == RoomTypeEnum.PRESIDENTIAL) {
            this.price = 1200;
        }

    }
}
