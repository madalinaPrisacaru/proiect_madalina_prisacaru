package wantsome.project.db.rooms.roomDto;

import java.util.Objects;

public class RoomDto{


    private final int id;
    private final int roomTypeId;
    private final String extraInfo;


    public RoomDto(int id, int roomTypeId, String extraInfo) {
        this.id = id;
        this.roomTypeId = roomTypeId;
        this.extraInfo = extraInfo;
    }

    public int getId() {
        return id;
    }

    public int getRoomTypeId() {
        return roomTypeId;
    }

    public String getExtraInfo() {
        return extraInfo;
    }


    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", roomTypeId=" + roomTypeId +
                ", extraInfo='" + extraInfo + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomDto)) return false;
        RoomDto room = (RoomDto) o;
        return getId() == room.getId() && getRoomTypeId() == room.getRoomTypeId()
                && Objects.equals(getExtraInfo(), room.getExtraInfo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRoomTypeId(), getExtraInfo());
    }
}
