package wantsome.project.db.rooms;

import wantsome.project.db.rooms.roomDto.RoomDto;

import java.util.Objects;

public class RoomWithTypeDto extends RoomDto{

    private final String typeDescription;
    private final double price;
    private final int capacity;
    private AvailabilityRoom availability;

    public RoomWithTypeDto(int id, int roomTypeId, String extraInfo,
                           String typeDescription, double price,
                           int capacity) {

        super(id, roomTypeId, extraInfo);
        this.typeDescription = typeDescription;
        this.price = price;
        this.capacity = capacity;
    }

    public double getPrice() {
        return price;
    }

    public int getCapacity() {
        return capacity;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setAvailability(AvailabilityRoom availability) {
        this.availability = availability;
    }

    public AvailabilityRoom getAvailability() {
        return availability;
    }

    @Override
    public String toString() {
        return "RoomWithTypeDto{" +
                "id=" + this.getId() +
                ", roomTypeId=" + this.getRoomTypeId() +
                ", extraInfo='" + this.getExtraInfo() + '\'' +
                ", typeDescription='" + typeDescription + '\'' +
                ", price=" + price +
                ", capacity=" + capacity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomWithTypeDto)) return false;
        RoomWithTypeDto that = (RoomWithTypeDto) o;
        return getId() == that.getId() &&
                this.getRoomTypeId() == that.getRoomTypeId() &&
                Double.compare(that.getPrice(), getPrice()) == 0 &&
                getCapacity() == that.getCapacity() &&
                Objects.equals(this.getExtraInfo(), that.getExtraInfo()) &&
                Objects.equals(getTypeDescription(), that.getTypeDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), this.getRoomTypeId(), this.getExtraInfo(), getTypeDescription(), getPrice(), getCapacity());
    }
}
