package wantsome.project.db.rooms;

public enum RoomTypeEnum{
    SINGLE("SINGLE"),
    DOUBLE("DOUBLE"),
    TRIPLE("TRIPLE"),
    PENTHOUSE("PENTHOUSE"),
    PRESIDENTIAL("PRESIDENTIAL");

    public final String label;

    RoomTypeEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static RoomTypeEnum parseEnum(final String roomType) {
        for (RoomTypeEnum type : RoomTypeEnum.values()) {
            if (type.label.equalsIgnoreCase(roomType)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid room type.");
    }
}


