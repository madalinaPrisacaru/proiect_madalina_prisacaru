package wantsome.project.db.users;

import wantsome.project.other.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    public List<UserDto> getAll() {
        String sql = "SELECT * FROM USERS;";

        List<UserDto> usersList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            try (ResultSet resultSet = statement.executeQuery()) {


                while (resultSet.next()) {

                    usersList.add(extractUsersFromResult(resultSet));
                }

            }


        } catch (SQLException e) {
            throw new RuntimeException("The list of users could not be loaded" + e.getMessage(), e);
        }


        return usersList;
    }


    private UserDto extractUsersFromResult(ResultSet rs) throws SQLException {

        int id = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String email = rs.getString("email");
        String password = rs.getString("password");


        return new UserDto(id, firstName, lastName, email, password);
    }

    public void insert(UserDto userDto) {

        String sql = "insert into USERS(FIRST_NAME ,LAST_NAME ,EMAIL ,PASSWORD) values (?, ?, ?, ?);";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            int idx = 0;

            ps.setString(++idx, userDto.getFirstName());
            ps.setString(++idx, userDto.getLastName());
            ps.setString(++idx, userDto.getEmail());
            ps.setString(++idx, userDto.getPassword());


            int affectedRows = ps.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        userDto.setId(generatedKeys.getInt(1));
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error saving new user" + e.getMessage(), e);
        }
    }

    public int getIDByEmailAndPassword(String email, String password) {

        String sql = "select ID from USERS \n" +
                "where EMAIL =? and password = ?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;
            ps.setString(++idx, email);
            ps.setString(++idx, password);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("id");
                }
            }
            return -1;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading user by email: " + e.getMessage(), e);
        }
    }

}
