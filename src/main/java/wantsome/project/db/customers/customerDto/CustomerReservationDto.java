package wantsome.project.db.customers.customerDto;

import wantsome.project.db.reservations.State;
import wantsome.project.db.rooms.RoomTypeEnum;

import java.sql.Date;
import java.util.Objects;

public class CustomerReservationDto {
    private final int id;
    private final RoomTypeEnum description;
    private final String extra_info;
    private final String first_name;
    private final String last_name;
    private final String email;
    private final String address;
    private final Date startDate;
    private final Date endDate;
    private final int roomId;
    private final State state;

    public CustomerReservationDto(int id, RoomTypeEnum description, String extra_info,
                                  String first_name, String last_name, String email,
                                  String address, Date startDate, Date endDate, int roomId, State state) {

        this.id = id;
        this.description = description;
        this.extra_info = extra_info;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.address = address;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomId = roomId;
        this.state = state;
    }


    public int getId() {
        return id;
    }

    public RoomTypeEnum getDescription() {
        return description;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getRoomId() {
        return roomId;
    }

    public State getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerReservationDto)) return false;
        CustomerReservationDto that = (CustomerReservationDto) o;
        return getId() == that.getId() && getRoomId() == that.getRoomId() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getExtra_info(), that.getExtra_info()) && Objects.equals(getFirst_name(), that.getFirst_name()) && Objects.equals(getLast_name(), that.getLast_name()) && Objects.equals(getEmail(), that.getEmail()) && Objects.equals(getAddress(), that.getAddress()) && Objects.equals(getStartDate(), that.getStartDate()) && Objects.equals(getEndDate(), that.getEndDate()) && getState() == that.getState();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getExtra_info(), getFirst_name(), getLast_name(), getEmail(), getAddress(), getStartDate(), getEndDate(), getRoomId(), getState());
    }

    @Override
    public String toString() {
        return "CustomerReservationDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", extra_info='" + extra_info + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", roomId=" + roomId +
                ", state=" + state +
                '}';
    }
}


