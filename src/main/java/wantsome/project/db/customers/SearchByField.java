package wantsome.project.db.customers;

public enum SearchByField {
    LAST_NAME,
    EMAIL
}
