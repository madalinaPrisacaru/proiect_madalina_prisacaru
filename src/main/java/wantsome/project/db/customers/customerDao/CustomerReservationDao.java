package wantsome.project.db.customers.customerDao;

import wantsome.project.db.customers.customerDto.CustomerReservationDto;
import wantsome.project.db.reservations.State;
import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.other.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerReservationDao {


    /***
     * Load list of reservations for customers from DB (all found)
     */

    public List<CustomerReservationDto> getCustomerReservations(int id) {

        String sql = "SELECT r.ID, rt.DESCRIPTION, r.EXTRA_INFO, c.FIRST_NAME, c.LAST_NAME, c.EMAIL, c.ADDRESS, r.START_DATE, r.END_DATE, r.ROOM_ID ,r.STATE\n" +
                " FROM CUSTOMER c JOIN RESERVATION r ON r.CUSTOMER_ID =c.ID  JOIN ROOM ro ON r.ROOM_ID= ro.ID JOIN ROOM_TYPE rt ON ro.ROOM_TYPE_ID=rt.ID\n" +
                " WHERE c.ID=?;";

        List<CustomerReservationDto> customer = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    customer.add(new CustomerReservationDto(
                            resultSet.getInt("ID"),
                            RoomTypeEnum.valueOf(resultSet.getString("DESCRIPTION")),
                            resultSet.getString("EXTRA_INFO"),
                            resultSet.getString("FIRST_NAME"),
                            resultSet.getString("LAST_NAME"),
                            resultSet.getString("EMAIL"),
                            resultSet.getString("ADDRESS"),
                            resultSet.getDate("START_DATE"),
                            resultSet.getDate("END_DATE"),
                            resultSet.getInt("ROOM_ID"),
                            State.valueOf(resultSet.getString("STATE"))));
                }
            }
            return customer;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading customers reservations!" + e.getMessage() + e);
        }

    }
}
