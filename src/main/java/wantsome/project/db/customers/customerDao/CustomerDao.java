package wantsome.project.db.customers.customerDao;

import wantsome.project.db.AbstractDao;
import wantsome.project.db.customers.SearchByField;
import wantsome.project.db.customers.customerDto.CustomerDto;
import wantsome.project.other.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerDao extends AbstractDao {

    private static final String QUERY_ALL_CUSTOMERS = "SELECT * FROM CUSTOMER ";

    /***
     * Load list of customers from DB (all found)
     */

    public List<CustomerDto> loadAll() {

        String sql = "SELECT * FROM CUSTOMER;";

        List<CustomerDto> customerList = new ArrayList<>();

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            try (ResultSet resultSet = statement.executeQuery()) {


                while (resultSet.next()) {

                    customerList.add(new CustomerDto(
                            resultSet.getInt("id"),
                            resultSet.getString("first_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("email"),
                            resultSet.getString("address")));
                }

            }


        } catch (SQLException e) {

            throw new RuntimeException("Error loading customer: " + e.getMessage());
        }

        return customerList;
    }

    /**
     * Load one specific customer from DB (by id)
     */

    public Optional<CustomerDto> loadById(int id) {

        String sql = "SELECT * FROM CUSTOMER WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet rs = statement.executeQuery()) {

                if (rs.next()) {
                    CustomerDto customer = extractCustomerFromResult(rs);
                    return Optional.of(customer);
                }
                return Optional.empty();
            }
        } catch (SQLException e) {

            throw new RuntimeException("Error loading customer by id: " + id + ":" + e.getMessage());
        }


    }


    /**
     * Auxiliary method
     */

    private CustomerDto extractCustomerFromResult(ResultSet rs) throws SQLException {

        return new CustomerDto(
                rs.getInt("ID"),
                rs.getString("FIRST_NAME"),
                rs.getString("LAST_NAME"),
                rs.getString("EMAIL"),
                rs.getString("ADDRESS")
        );
    }


    /**
     * Add new customer to DB
     */

    public void insert(CustomerDto customer) {

        String sql = "INSERT INTO CUSTOMER(FIRST_NAME, LAST_NAME, EMAIL, ADDRESS) VALUES(?,?,?,?);";


        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            int idx = 0;


            statement.setString(++idx, customer.getFirst_name());
            statement.setString(++idx, customer.getLast_name());
            statement.setString(++idx, customer.getEmail());
            statement.setString(++idx, customer.getAddress());

            statement.execute();

        } catch (SQLException e) {

            throw new RuntimeException("Error insert new customer: " + e.getMessage());
        }
    }


    /**
     * Update an existing customer in DB(byLastName)
     */

    public void updateById(int id, String firstName, String lastName, String email, String address) {
        String sql = "UPDATE CUSTOMER SET FIRST_NAME=?, LAST_NAME =?, EMAIL=?, ADDRESS=? WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, email);
            statement.setString(4, address);
            statement.setInt(5, id);
            statement.executeUpdate();

        } catch (SQLException e) {

            System.err.println("Error update customer: " + e.getMessage());
        }

    }

    /**
     * Delete an existing customer from DB
     */

    public static void delete(int id) {

        String sql = "DELETE FROM CUSTOMER WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);
            statement.execute();

        } catch (SQLException e) {

            throw new RuntimeException("Error delete customer: " + e.getMessage());
        }

    }

    /**
     * Search the database after customer last name or email
     */

    public List<CustomerDto> search(SearchByField searchByField, String searchText) {
        String filterByQuery = " WHERE " + searchByField.name() + " LIKE '%" + searchText + "%'";

        List<CustomerDto> customerList = new ArrayList<>();

        final String finalQuery = QUERY_ALL_CUSTOMERS + filterByQuery;
        System.out.println(finalQuery);

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(finalQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    customerList.add(extractCustomerFromResult(resultSet));
                }
            }

        } catch (SQLException e) {

            throw new RuntimeException("Error loading customers: " + e.getMessage());
        }

        return customerList;

    }

}

