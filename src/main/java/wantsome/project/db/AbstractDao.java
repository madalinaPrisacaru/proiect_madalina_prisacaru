package wantsome.project.db;

import wantsome.project.db.reservations.OrderSortingType;


public abstract class AbstractDao{
    private static final String AND = "AND";
    private static final String OR =  "OR";
    private static final String ORDER_BY = "ORDER BY";
    private static final String WHERE = "WHERE";
    private static final String SPACE = " ";
    private static final String PARAM = "?";
    private static final String EQUALS_PARAM = "=" + PARAM;
    private static final String COMMA = ",";

    protected void addFilter(StringBuilder queryBuilder, int paramNumber, String paramIdentifier) {
        if (paramNumber > 0) {
            if (queryBuilder.toString().contains(PARAM)) {
                queryBuilder.append(SPACE + AND);
            } else {
                queryBuilder.append(SPACE + WHERE);
            }
            queryBuilder.append(SPACE).append(paramIdentifier).append(SPACE + EQUALS_PARAM);
            for (int i = 0; i < paramNumber - 1; i++) {
                queryBuilder.append(SPACE + OR).append(paramIdentifier).append(SPACE + EQUALS_PARAM);
            }
        }
    }

    protected void addSorting(StringBuilder queryBuilder, String paramIdentifier, OrderSortingType order) {
        if (queryBuilder.toString().contains(ORDER_BY)) {
            queryBuilder.append(COMMA);
        } else {
            queryBuilder.append(SPACE + ORDER_BY);
        }
        queryBuilder.append(SPACE).append(paramIdentifier).append(SPACE).append(order.getLabel());
    }
}
