package wantsome.project.db.reservations.reservationDao;

import wantsome.project.db.AbstractDao;
import wantsome.project.db.reservations.OrderSortingType;
import wantsome.project.db.reservations.SearchByField;
import wantsome.project.db.reservations.State;
import wantsome.project.db.reservations.reservationDto.ReservationDto;
import wantsome.project.db.reservations.reservationDto.ReservationFullDetails;
import wantsome.project.db.rooms.RoomTypeEnum;
import wantsome.project.other.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReservationDao extends AbstractDao {

    private static final String QUERY_ALL_RESERVATIONS = "SELECT * FROM RESERVATION ";

    /***
     * Load  list of reservations from DB (all found)
     */

    public List<ReservationDto> loadAll(OrderSortingType order, State state) {
        String filterByQuery = getFilterByQuery(state);
        String orderByQuery = getOrderByQuery(order);

        List<ReservationDto> reservationList = new ArrayList<>();

        final String finalQuery = QUERY_ALL_RESERVATIONS + filterByQuery + orderByQuery;

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(finalQuery)) {

            setFilterByParameters(statement, state);

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    reservationList.add(extractReservationsFromResult(resultSet));
                }

            }


        } catch (SQLException e) {

            System.err.println("Error loading reservations: " + e.getMessage());
        }

        return reservationList;
    }

    public List<ReservationDto> loadAllReservations() {


        List<ReservationDto> reservationList = new ArrayList<>();


        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_ALL_RESERVATIONS)) {


            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    reservationList.add(extractReservationsFromResult(resultSet));
                }

            }


        } catch (SQLException e) {

            System.err.println("Error loading reservations: " + e.getMessage());
        }

        return reservationList;
    }

    private void setFilterByParameters(PreparedStatement statement, State state) throws SQLException {
        if (state != null) {
            statement.setString(1, state.getLabel());
        }
    }

    private String getFilterByQuery(State state) {
        StringBuilder builder = new StringBuilder();
        if (state != null) {
            this.addFilter(builder, 1, "STATE");
        }
        return builder.toString();
    }

    private String getOrderByQuery(OrderSortingType order) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (order != null) {
            this.addSorting(stringBuilder, "START_DATE", order);
            this.addSorting(stringBuilder, "END_DATE", order);
        }
        return stringBuilder.toString();
    }


    /**
     * Load list of reservations from DB (full details)
     */

    public List<ReservationFullDetails> loadReservationFullDetails(OrderSortingType order, State state) {

        String sql = "\n" +
                "SELECT r.ID, c.FIRST_NAME, c.LAST_NAME, r.START_DATE, r.END_DATE, r.ROOM_ID, rt.DESCRIPTION, \n" +
                "rt.PRICE, r.EXTRA_INFO, r.STATE FROM RESERVATION r \n" +
                " JOIN CUSTOMER c ON r.CUSTOMER_ID =c.ID \n" +
                " JOIN ROOM r2 ON r.ROOM_ID =r2.ID \n" +
                " JOIN ROOM_TYPE rt ON r2.ROOM_TYPE_ID =rt.ID ";

        String filterByQuery = getFilterByQuery(state);
        String orderByQuery = getOrderByQuery(order);

        List<ReservationFullDetails> reservationList = new ArrayList<>();

        final String finalQuery = sql + filterByQuery + orderByQuery;

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(finalQuery)) {


            setFilterByParameters(statement, state);


            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {
                    reservationList.add(new ReservationFullDetails(
                            resultSet.getInt("ID"),
                            resultSet.getString("FIRST_NAME"),
                            resultSet.getString("LAST_NAME"),
                            resultSet.getDate("START_DATE"),
                            resultSet.getDate("END_DATE"),
                            resultSet.getInt("ROOM_ID"),
                            RoomTypeEnum.valueOf(resultSet.getString("DESCRIPTION")),
                            resultSet.getDouble("PRICE"),
                            resultSet.getString("EXTRA_INFO"),
                            State.valueOf(resultSet.getString("STATE")
                            )
                    ));
                }

            }
            return reservationList;

        } catch (SQLException e) {

            System.err.println("Error loading reservations: " + e.getMessage());
        }


        return reservationList;
    }


    /**
     * Load list of reservations from DB by id (all found)
     */

    public Optional<ReservationDto> loadById(int id) {

        String sql = "SELECT * FROM RESERVATION WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {

                if (resultSet.next()) {
                    ReservationDto reservationDto = extractReservationsFromResult(resultSet);
                    return Optional.of(reservationDto);
                }

                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error loading room_type by id " + id + ": " + e.getMessage());
        }

    }

    /***
     * Add a new reservations to DB,
     * only if that date is available
     */

    public void insertReservation(ReservationDto res) {

        String sql = "INSERT INTO RESERVATION(CUSTOMER_ID, START_DATE, END_DATE, ROOM_ID, EXTRA_INFO, STATE ) VALUES(?,?,?,?,?,?);";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {


            int idx = 0;
            statement.setInt(++idx, res.getCustomerId());
            statement.setDate(++idx, res.getStartDate());
            statement.setDate(++idx, res.getEndDate());
            statement.setInt(++idx, res.getRoomId());
            statement.setString(++idx, res.getExtra_info());
            statement.setObject(++idx, res.getState());

            Date start_date = res.getStartDate();

            Date end_date = res.getEndDate();

            int roomId=res.getRoomId();

            boolean validateReservation = validationInsertANewReservation(roomId,start_date, end_date);

            if (!validateReservation) {
                throw new RuntimeException("Reservation unavailable! Try another date");
            }

            int modifiedRows = statement.executeUpdate();
            if (modifiedRows > 0) {
                try (ResultSet resultSetKeys = statement.getGeneratedKeys()) {
                    if (resultSetKeys.next()) {

                        int id = resultSetKeys.getInt(1);
                        res.setId(id);
                    }
                }
            }


        } catch (SQLException e) {

            throw new RuntimeException("Error saving new reservations: " + e.getMessage());
        }

    }

    /**
     * Auxiliary method
     */

    private ReservationDto extractReservationsFromResult(ResultSet resultSet) throws SQLException {
        return new ReservationDto(


                resultSet.getInt("customer_id"),
                resultSet.getDate("start_date"),
                resultSet.getDate("end_date"),
                resultSet.getInt("room_id"),
                resultSet.getString("extra_info"),
                State.valueOf(resultSet.getString("state")));
    }

    /***
     * Validation for inserting a new reservation, if it overlaps with another, then it will not be accepted
     */

    public boolean validationInsertANewReservation(int roomId, Date start_date, Date end_date) {

        List<ReservationDto> resList = loadAll(null, null);
        return resList.stream()
                .noneMatch(res ->
                        res.getRoomId() == roomId
                                && start_date.getTime() <= res.getEndDate().getTime()
                                && end_date.getTime() >= res.getStartDate().getTime());


    }

    /**
     * Delete a reservations from DB (by id)
     */

    public static void deleteReservation(int id) {

        String sql = "DELETE FROM RESERVATION WHERE RESERVATION.ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            statement.setInt(1, id);

            statement.executeUpdate();


        } catch (SQLException e) {

            throw new RuntimeException("Error deleting reservation: " + e.getMessage());
        }

    }

    /**
     * Update a reservations from DB only if it doesn't start and end in the past
     */

    public void update(int id, Date startDate, Date endDate, int roomId, String extraInfo, State state) {

        Date date = new Date(System.currentTimeMillis());

        if ((startDate.before(date) || (date.after(endDate)))) {

            System.err.println("This reservation can't be update!");
        }

        String sql = "UPDATE RESERVATION SET " +
                " START_DATE=?, " +
                " END_DATE=?, " +
                " ROOM_ID=?," +
                " EXTRA_INFO=?," +
                " STATE=?" +
                " WHERE ID=?;";

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {


            statement.setDate(1, startDate);
            statement.setDate(2, endDate);
            statement.setInt(3, roomId);
            statement.setString(4, extraInfo);
            statement.setString(5, state.name());
            statement.setInt(6, id);


            statement.executeUpdate();


        } catch (SQLException e) {

            throw new RuntimeException("Error updating reservations, can't update this reservation, isn't eligible: " + e.getMessage());
        }

    }


    /**
     * Search the database after reservation id, room id and customer id
     */

    public List<ReservationFullDetails> search(SearchByField searchByField, String searchText) {
        String sql = "\n" +
                "SELECT r.ID, c.FIRST_NAME, c.LAST_NAME, r.START_DATE, r.END_DATE, r.ROOM_ID, rt.DESCRIPTION, \n" +
                "rt.PRICE, r.EXTRA_INFO, r.STATE FROM RESERVATION r \n" +
                " JOIN CUSTOMER c ON r.CUSTOMER_ID =c.ID \n" +
                " JOIN ROOM r2 ON r.ROOM_ID =r2.ID \n" +
                " JOIN ROOM_TYPE rt ON r2.ROOM_TYPE_ID =rt.ID ";

        String filterByQuery = " WHERE r. " + searchByField.name() + " LIKE '%" + searchText + "%'";

        List<ReservationFullDetails> reservationList = new ArrayList<>();

        final String finalQuery = sql + filterByQuery;

        System.out.println(finalQuery);

        try (Connection connection = ConnectionPool.getConnection();
             PreparedStatement statement = connection.prepareStatement(finalQuery)) {

            try (ResultSet resultSet = statement.executeQuery()) {

                while (resultSet.next()) {

                    reservationList.add(new ReservationFullDetails(
                            resultSet.getInt("ID"),
                            resultSet.getString("FIRST_NAME"),
                            resultSet.getString("LAST_NAME"),
                            resultSet.getDate("START_DATE"),
                            resultSet.getDate("END_DATE"),
                            resultSet.getInt("ROOM_ID"),
                            RoomTypeEnum.valueOf(resultSet.getString("DESCRIPTION")),
                            resultSet.getDouble("PRICE"),
                            resultSet.getString("EXTRA_INFO"),
                            State.valueOf(resultSet.getString("STATE")
                            )
                    ));
                }

            }


        } catch (SQLException e) {

            throw new RuntimeException("Error loading reservations search: " + e.getMessage());
        }

        return reservationList;
    }


}