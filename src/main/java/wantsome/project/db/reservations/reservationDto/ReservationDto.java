package wantsome.project.db.reservations.reservationDto;

import wantsome.project.db.reservations.State;

import java.sql.Date;
import java.util.Objects;


public class ReservationDto {

    private int id;
    private final int customerId;
    private final Date startDate;
    private final Date endDate;
    private final int roomId;
    private final String extra_info;
    private final State state;


    public ReservationDto(int customer_id, Date start_date,
                          Date end_date, int room_id,
                          String extra_info, State state) {

        this.customerId = customer_id;
        this.startDate = start_date;
        this.endDate = end_date;
        this.roomId = room_id;
        this.extra_info = extra_info;
        this.state = state;
    }

    public ReservationDto(int id, int customerId,
                          Date startDate, Date endDate,
                          int roomId, String extraInfo,
                          State state) {
        this.id = id;
        this.customerId = customerId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomId = roomId;
        this.extra_info = extraInfo;
        this.state = state;
    }


    public int getId() {
        return id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getRoomId() {
        return roomId;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public State getState() {
        return state;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "ReservationDto{" +
                "id=" + id +
                ", costumer_id=" + customerId +
                ", start_date=" + startDate +
                ", end_date=" + endDate +
                ", room_id=" + roomId +
                ", extra_info='" + extra_info + '\'' +
                ", state=" + state +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationDto)) return false;
        ReservationDto that = (ReservationDto) o;
        return getId() == that.getId() && getCustomerId() == that.getCustomerId() &&
                getRoomId() == that.getRoomId() && Objects.equals(getStartDate(),
                that.getStartDate()) && Objects.equals(getEndDate(),
                that.getEndDate()) && Objects.equals(getExtra_info(),
                that.getExtra_info()) && getState() == that.getState();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCustomerId(), getStartDate(),
                getEndDate(), getRoomId(), getExtra_info(), getState());
    }
}
