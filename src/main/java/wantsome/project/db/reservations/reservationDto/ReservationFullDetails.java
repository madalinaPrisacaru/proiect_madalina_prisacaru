package wantsome.project.db.reservations.reservationDto;

import wantsome.project.db.reservations.State;
import wantsome.project.db.rooms.RoomTypeEnum;

import java.sql.Date;
import java.util.Objects;

public class ReservationFullDetails {
    private final int id;
    private final String first_name;
    private final String last_name;
    private final Date startDate;
    private final Date endDate;
    private final int roomId;
    private final RoomTypeEnum description;
    private final double price;
    private final String extra_info;
    private final State state;

    public ReservationFullDetails(int id, String first_name, String last_name,
                                  Date startDate, Date endDate, int roomId,
                                  RoomTypeEnum description, double price,
                                  String extraInfo, State state) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomId = roomId;
        this.description = description;
        this.price = price;
        this.extra_info = extraInfo;
        this.state = state;
    }


    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getRoomId() {
        return roomId;
    }

    public RoomTypeEnum getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "ReservationFullDetails{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", roomId=" + roomId +
                ", description=" + description +
                ", price=" + price +
                ", extraInfo='" + extra_info + '\'' +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationFullDetails)) return false;
        ReservationFullDetails that = (ReservationFullDetails) o;
        return getId() == that.getId() && getRoomId() == that.getRoomId() &&
                Double.compare(that.getPrice(), getPrice()) == 0 &&
                Objects.equals(getFirst_name(), that.getFirst_name()) &&
                Objects.equals(getLast_name(), that.getLast_name()) &&
                Objects.equals(getStartDate(), that.getStartDate()) &&
                Objects.equals(getEndDate(), that.getEndDate()) &&
                getDescription() == that.getDescription() &&
                Objects.equals(getExtra_info(), that.getExtra_info()) &&
                getState() == that.getState();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirst_name(), getLast_name(),
                getStartDate(), getEndDate(), getRoomId(), getDescription(),
                getPrice(), getExtra_info(), getState());
    }
}