package wantsome.project.db.reservations;

public enum OrderSortingType{
    ASC("ASC"),
    DESC("DESC");

    private final String label;

    OrderSortingType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
