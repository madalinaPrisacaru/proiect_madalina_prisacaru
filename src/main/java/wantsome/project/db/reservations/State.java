package wantsome.project.db.reservations;

public enum State{
    PAID("PAID"),
    CANCELED("CANCELED"),
    ACTIVE("ACTIVE");

    private final String label;

    State(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
