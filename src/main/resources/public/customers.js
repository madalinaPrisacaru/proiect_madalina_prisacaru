  function onLoadCustomersJS() {
      searchCustomerBindOnKeyPressEvent();
  }


  function searchCustomerBindOnKeyPressEvent() {
      var searchText = document.getElementById("searchText");
      searchText.addEventListener("keypress", function(event) {
          // If the user presses the "Enter" key on the keyboard
          if (event.key === "Enter") {
              // Cancel the default action, if needed
              event.preventDefault();

              var searchField = document.getElementById("searchByField");
              var url = "http://localhost:8080/customers"
              if (searchText.value) {
                  window.location.href = url + "?searchField=" + searchField.value +  "&searchText=" + searchText.value;
              } else {
                  window.location.href = url;
              }
          }
      });
  }