  function onLoadReservationsJS() {
       filterByStateBindOnChange();
       orderByDateBindOnChange();
       searchReservationsBindOnKeyPressEvent();
  }

  function filterByStateBindOnChange() {
      var stateDropdown = document.getElementById("filterByState");
      stateDropdown.onchange = function() {
          var url = "/reservations"
          var orderByDateParam = getOrderByDateSelectedValueAsParameter();
          if (stateDropdown.value != "ALL") {
              url += "?state=" + stateDropdown.value
              url += "&" + orderByDateParam;
          } else {
              url += "?" + orderByDateParam;
          }
          window.location.href = "http://localhost:8080" + url;
      }
  }

  function orderByDateBindOnChange() {
      var orderByDateDropdown = document.getElementById("orderByDate");
      orderByDateDropdown.onchange = function() {
          var url = "/reservations?order=" + orderByDateDropdown.value;
          var filterByStateParam = getFilterByStateSelectedValueAsParameter();
          if (filterByStateParam != null) {
              url += "&" + filterByStateParam;
          }
          window.location.href = "http://localhost:8080" + url;
      }
  }

  function getOrderByDateSelectedValueAsParameter() {
      return "order=" + document.getElementById("orderByDate").value;
  }

  function getFilterByStateSelectedValueAsParameter() {
      var stateDropdown = document.getElementById("filterByState");
      var selectedStateValue = stateDropdown.value;
      return selectedStateValue !== "ALL" ? "state=" + selectedStateValue : null;
  }

  function searchReservationsBindOnKeyPressEvent() {
      var searchText = document.getElementById("searchText");
      searchText.addEventListener("keypress", function(event) {
          // If the user presses the "Enter" key on the keyboard
          if (event.key === "Enter") {
              // Cancel the default action, if needed
              event.preventDefault();

              var searchField = document.getElementById("searchByField");
              var url = "http://localhost:8080/reservations"
              if (searchText.value) {
                  window.location.href = url + "?searchField=" + searchField.value + "&searchText=" + searchText.value;
              } else {
                  window.location.href = url;
              }
          }
      });
  }