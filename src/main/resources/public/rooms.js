  function onLoadRoomsJS() {
       filterByRoomTypeBindOnChange();

  }

  function filterByRoomTypeBindOnChange() {
      var stateDropdown = document.getElementById("filterByRoomType");
      stateDropdown.onchange = function() {
          var url = "/rooms"
          if (stateDropdown.value != "ALL") {
              url += "?type=" + stateDropdown.value
          }
          window.location.href = "http://localhost:8080" + url;
      }
  }
