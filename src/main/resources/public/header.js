function onAnimationHeaderJS() {
    headerElementOnClick();
    headerElementHidden();
  }

    function headerElementOnClick() {
      var x = document.getElementById("navbar");
      if (x.className === "navbar") {
        x.className += " responsive";
      } else {
        x.className = "navbar";
      }
    }

    function headerElementHidden() {
      var prevScrollpos = window.pageYOffset;
      window.onscroll = function () {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
          document.getElementById("header").style.top = "0";
        } else {
          document.getElementById("header").style.top = "-110px";
        }
        prevScrollpos = currentScrollPos;
      };
}



